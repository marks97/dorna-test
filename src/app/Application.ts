export default class Application {

    private server;

    constructor({ server }) {
        this.server = server;
    }

    async start() {
        await this.server.start();
    }
}