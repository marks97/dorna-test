import StateRepository from '@infra/state/stateRepository'
import Simulation from '@models/Simulation'

const simulationData = StateRepository.getSimulationData();

export default class ClientController {

    private simulation;
    private clients;
    private messageService;

    constructor() {
        this.simulation = new Simulation(simulationData);
        this.clients = [];
    }

    public onClientMessage(client, message) {

        console.log(message);

        const index = this.clients.indexOf(client);
        const command = message.command;

        switch (command) {
            case "play": {
                this.clients[index].active = true;
                if (this.activeClients() === 1) {
                    this.startMessageService();
                }
                break;
            }
            case "stop": {
                this.clients[index].active = false;
                if (this.activeClients() === 0) {
                    this.stopMessageService();
                }
                break;
            }
            case "reset": {
                this.clients[index].send(JSON.stringify({ kind: "data", data: this.simulation.data[0]}));
                this.clients[index].index = 1;
                this.clients[index].active = false;
                break;
            }
        }
    }

    public onClientConnect(client) {
        client.send(JSON.stringify({ kind: "data", data: this.simulation.data[0]}));
        client.index = 1;
        client.active = false;
        this.clients.push(client);
    }

    public onClientDisconnect(client) {
        const index = this.clients.indexOf(client);
        this.clients.splice(index, 1);
        if (this.clients.length === 0) {
            this.stopMessageService();
        }
    }


    // Starts an interval that sends its corresponding data to clients every 50 ms
    private startMessageService() {
        const context = this;
        this.messageService = setInterval(function () {
            for(let client of context.clients) {
                const state = context.simulation.data[client.index];
                if (client.active) {
                    client.send(JSON.stringify({
                        kind: "data",
                        data: state
                    }));
                    client.index++;

                    // Checks if it's the last data available. If it's the case we send stop command to the client.
                    if (client.index >= context.simulation.data.length) {
                        client.send(JSON.stringify({
                            kind: "status",
                            data: {
                                status: "stop"
                            }
                        }));
                    }

                }
            }
        }, 50);
    }

    private stopMessageService() {
        console.log("\nNo active clients. Stopping message service.");
        clearInterval(this.messageService);
    }

    private activeClients() : number {
        const activeClients = this.clients.filter(client => client.active);
        return activeClients.length;
    }

}