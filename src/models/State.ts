export default class State {

    public time: Date;
    public gear: string;
    public rpm: number;
    public speed: number;

    constructor(state : any) {
        this.time = new Date(state.time);
        this.gear = state.gear;
        this.rpm = state.rpm;
        this.speed = state.speed;
    }

}