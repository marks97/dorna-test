import State from "@models/State";

export default class Simulation {

    public startTime: Date;
    public data: State[];

    constructor(simulation : any) {
        this.startTime = new Date(simulation.startTime);
        this.data = simulation.data.map((state) => {
            return new State(state);
        })
    }

}