import 'module-alias/register';
import express from 'express';
import Application from '@app/Application'
import Server from '@interfaces/http/Server'

const server = new Server(express());
const app = new Application({server});

app
    .start()
    .catch((error) => {
        console.log(error);
        process.exit();
    });
