import * as path from "path";
import * as http from 'http';
import * as WebSocket from 'ws';

import ClientController from "@app/ClientController";

const PORT = process.env.PORT || '3000';
const PUBLIC_DIR = process.env.PUBLIC_DIR || './src/public';


export default class Server {

    private express;
    private server;
    private ws;

    private clientController;

    constructor(express) {
        this.express = express;
        this.server = http.createServer(this.express);
        this.clientController = new ClientController();
    }

    start() {

        this.express.get('/', (req, res) => {
            res.sendFile('frontend.html', {
                root: path.join(PUBLIC_DIR)
            });
        });

        this.server.listen(PORT, err => {
            if (err) {
                return console.error(err);
            }
            return console.log('\nServer is listening on ' + PORT);
        });

        this.ws = new WebSocket.Server( { server: this.server, path: '/replay'} );
        this.ws.on('connection', (client) => {
            this.clientController.onClientConnect(client);
            client.on('message', message => {
                this.clientController.onClientMessage(client, JSON.parse(message));
            });
            client.on('close', () => {
                this.clientController.onClientDisconnect(client);
            });
        });

    }

}