import Simulation from "@models/Simulation";
import * as simulationFile from '@assets/simfile.json'

export default class StateRepository {

    static getSimulationData() {
        return new Simulation(simulationFile);
    }

}