import { expect } from 'chai'
import 'mocha'
import 'module-alias/register';

import State from '@models/State'
import StateRepository from "@infra/state/stateRepository";

describe('Infra :: State :: StateRepository', () => {

    describe('#getSimulationData', () => {
        it('returns all data from the simulation file', async () => {

            const simulation = await StateRepository.getSimulationData();
            const states = simulation.data;

            expect(simulation).to.have.property('startTime');
            expect(simulation.startTime).to.be.an('date');
            expect(states).to.be.an('array');
            expect(states).to.not.be.empty;
            expect(states[0]).to.be.instanceOf(State);
        });
    });
});