import { expect } from 'chai'
import 'mocha'
import 'module-alias/register';

import request from 'request'
import WebSocket from 'ws'

const PORT = process.env.PORT || '3000';

describe('Interfaces :: HTTP :: Server', () => {


    let response;
    before(async () => {
        return new Promise((resolve) => {
            request('http://0.0.0.0:' + PORT, function (error, res) {
                response = res;
                resolve();
            })
        });
    });

    describe('#Get main content', () => {
        it('gets an html page', async () => {

            expect(response).to.not.be.undefined;
            expect(response.statusCode).to.eql(200);
            expect(response.body).to.not.be.empty;

        });
    });

    let resData;
    before(async () => {
        return new Promise((resolve) => {
            const ws = new WebSocket('ws://0.0.0.0:' + PORT + '/replay');
            ws.on('open', () => {
                ws.on('message', res => {
                    resData = JSON.parse(res);
                    resolve();
                });
            });
        });
    });

    describe('#Websocket test', () => {
        it('connects to websocket successfully', async () => {

            expect(resData).to.have.ownProperty('kind');
            expect(resData).to.have.ownProperty('data');

        });
    });

});